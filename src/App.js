import React from "react";
import {useState} from "react";

/**
 * If count is 0 display "POOP_TEXT"
 * If count is divisible by 3 display "COUNTER_TEXT"
 * If count is divisible by 5 display "FOO_TEXT"
 * If count is divisible by 3 && 5 display "COUNTER_FOO_TEXT"
 * If count is not divisible by 3, 5 display "POOP_TEXT"
 * The increment button should increase the "count" variable :done
 * The decrement button should decrease the "count" variable :done 
 * The "count" variable should never go below 0  :done 
 */

const COUNTER_TEXT = "Counter";
const FOO_TEXT = "Foo";
const COUNTER_FOO_TEXT = "🙅🏿‍♂️ Counter Foo 🙅🏿‍♂️";
const POOP_TEXT = "💩";




function App() {
  
  const [count, setCount] = useState(0)
  // count = 0;
  let text = POOP_TEXT;

  const PoopOrNoPoop =() =>{
    if(count<1){
    return <h1>{POOP_TEXT}</h1>
    }return null
  }
    

  return (
    <div style={styles.app}>
      <h1>{count}</h1>
      <PoopOrNoPoop/>
      <div style={styles.buttons}>
        <button onClick={() => setCount(count + 1)} >Increment</button>
        <button onClick={() => { if(count>0) {setCount(count - 1)}}}  >Decrement</button>
      </div>
    </div>
  );
}



const styles = {
  app: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
  },
};
export default App;
